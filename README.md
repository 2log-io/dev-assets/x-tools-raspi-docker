# X-Toolchains for RaspberryPi

Allows building docker based crosstoolchains for the Raspberry Pi. Especially relevant for Apple M1 users, since there are almost no aarch64 toolchains for the raspi available.


